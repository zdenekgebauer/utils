<?php

declare(strict_types=1);

namespace Tests\Unit;

use Codeception\Test\Unit;
use Tests\Support\UnitTester;
use ZdenekGebauer\Utils\Vars\StringUtils;

use function strlen;

class StringUtilsTest extends Unit
{
    protected UnitTester $tester;

    public function testKeywords(): void
    {
        $this->tester->assertEquals([], StringUtils::keywords(''));
        $this->tester->assertEquals(['aaaaa', 'bbbbbb'], StringUtils::keywords('aa aaaaa bbbbbb'));
        $this->tester->assertEquals(['bbbbbb'], StringUtils::keywords('aa aaaaa bbbbbb', 6));
    }

    public function testMaxLength(): void
    {
        $this->tester->assertEquals('lorem ipsum', StringUtils::maxLength('lorem ipsum', 20));
        $this->tester->assertEquals('lorem', StringUtils::maxLength('lorem ipsum', 5));
        $this->tester->assertEquals(
            'příliš žluťoučký kůň',
            StringUtils::maxLength('příliš žluťoučký kůň úpěl ďábelské ódy', 20)
        );
    }

    public function testPerex(): void
    {
        $str = 'escrzyaie';
        $this->tester->assertEquals('escrzyaie', StringUtils::perex($str, 2));
        $this->tester->assertEquals('escr', StringUtils::perex($str, 2, 2));

        $add = '...';
        $this->tester->assertEquals('escr' . $add, StringUtils::perex($str, 2, 2, $add));
    }

    public function testRandomString(): void
    {
        $this->tester->assertEquals(3, strlen(StringUtils::randomString(3)));
    }

    public function testSlugify(): void
    {
        $this->tester->assertEquals('escrzyaie', StringUtils::slugify('ěščřžýáíé'));
        $this->tester->assertEquals('escrzyaie', StringUtils::slugify('ĚŠČŘŽÝÁÍÉ'));
        $this->tester->assertEquals('a-b', StringUtils::slugify('A  B'));
        $this->tester->assertEquals('a-b', StringUtils::slugify('A--B'));
    }

    public function testStrPadCut(): void
    {
        $this->tester->assertEquals(' 123', StringUtils::strPadCut('123', 4, ' ', STR_PAD_LEFT));
        $this->tester->assertEquals('12', StringUtils::strPadCut('123', 2, ' ', STR_PAD_LEFT));
        $this->tester->assertEquals('123 ', StringUtils::strPadCut('123', 4, ' ', STR_PAD_RIGHT));
        $this->tester->assertEquals('12', StringUtils::strPadCut('123', 2, ' ', STR_PAD_RIGHT));
    }

    public function testSubstr(): void
    {
        $str = 'escrzyaie';
        $strB = 'ěščřžýáíé';
        $this->tester->assertEquals('es', StringUtils::substr($str, 0, 2));
        $this->tester->assertEquals('ěš', StringUtils::substr($strB, 0, 2, 'UTF-8'));
        $this->tester->assertEquals('čř', StringUtils::substr($strB, 2, 2, 'UTF-8'));
        $this->tester->assertEquals('ie', StringUtils::substr($str, -2));
        $this->tester->assertEquals('íé', StringUtils::substr($strB, -2, null, 'UTF-8'));
    }
}
