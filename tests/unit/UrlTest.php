<?php

declare(strict_types=1);

namespace Tests\Unit;

use Codeception\Exception\Warning;
use Codeception\Test\Unit;
use Tests\Support\UnitTester;
use ZdenekGebauer\Utils\Html\Url;

class UrlTest extends Unit
{
    protected UnitTester $tester;

    public function testQueryUrl(): void
    {
        $url = 'http://example.org';
        $this->tester->assertEquals('http://example.org', Url::queryUrl($url, []));
        $values = ['a' => 'A', 'b' => 'B'];
        $this->tester->assertEquals('http://example.org?a=A&b=B', Url::queryUrl($url, $values));
        $values = ['a' => 'A', 'b' => 'B', 'c', 1 => 2, 'd' => null];
        $this->tester->assertEquals('http://example.org?a=A&b=B&0=c&1=2', Url::queryUrl($url, $values));
        $values = ['a' => ['10', '20']];
        $this->tester->assertEquals(
            'http://example.org?a' . urlencode('[0]') . '=10&a' . urlencode('[1]') . '=20',
            Url::queryUrl($url, $values)
        );
    }

    public function testRemoveAbsUrl(): void
    {
        $html = '<a href="https://example.org/dir/file.htm">link</a> <img src="https://example.org/dir/file/image" />';
        $expect = '<a href="dir/file.htm">link</a> <img src="dir/file/image" />';
        $this->tester->assertEquals($expect, Url::removeAbsUrl($html, 'https://example.org/'));
    }

    public function testToDataUri(): void
    {
        $this->tester->assertStringStartsWith('data:text/x-php;base64,', Url::toDataUri(__FILE__));
        $this->tester->assertEquals('', Url::toDataUri(''));
    }

    /**
     * @env lando
     */
    public function testToDataUriFail(): void
    {
        $this->tester->expectThrowable(Warning::class, static function () {
            Url::toDataUri('file_not_exists');
        });
    }

    public function testUrlToAbs(): void
    {
        $html = '<a href="dir/file.htm">link</a> <img src="dir/file/image" />';
        $expect = '<a href="http://example.org/dir/file.htm">link</a> <img src="http://example.org/dir/file/image" />';
        $this->tester->assertEquals($expect, Url::urlToAbs($html, 'http://example.org/'));
    }
}
