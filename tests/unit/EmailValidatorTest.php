<?php

declare(strict_types=1);

namespace Tests\Unit;

use Codeception\Test\Unit;
use Tests\Support\UnitTester;
use ZdenekGebauer\Utils\Email\EmailValidator;

class EmailValidatorTest extends Unit
{
    protected UnitTester $tester;

    public function provideEmailsWithGenericDomain(): array
    {
        $emails = [
            'address@somewhere.academy',
            'address@somewhere.aero',
            'address@somewhere.asia',
            'address@somewhere.bike',
            'address@somewhere.buzz',
            'address@somewhere.camera',
            'address@somewhere.camp',
            'address@somewhere.careers',
            'address@somewhere.center',
            'address@somewhere.clothing',
            'address@somewhere.company',
            'address@somewhere.computer',
            'address@somewhere.construction',
            'address@somewhere.contractors',
            'address@somewhere.coop',
            'address@somewhere.diamonds',
            'address@somewhere.directory',
            'address@somewhere.domains',
            'address@somewhere.enterprises',
            'address@somewhere.equipment',
            'address@somewhere.estate',
            'address@somewhere.gallery',
            'address@somewhere.graphics',
            'address@somewhere.guru',
            'address@somewhere.holdings',
            'address@somewhere.info',
            'address@somewhere.jobs',
            'address@somewhere.kitchen',
            'address@somewhere.land',
            'address@somewhere.lighting',
            'address@somewhere.limo',
            'address@somewhere.management',
            'address@somewhere.menu',
            'address@somewhere.mobi',
            'address@somewhere.museum',
            'address@somewhere.name',
            'address@somewhere.photography',
            'address@somewhere.photos',
            'address@somewhere.plumbing',
            'address@somewhere.ruhr',
            'address@somewhere.sexy',
            'address@somewhere.shoes',
            'address@somewhere.singles',
            'address@somewhere.support',
            'address@somewhere.systems',
            'address@somewhere.tattoo',
            'address@somewhere.technology',
            'address@somewhere.tips',
            'address@somewhere.today',
            'address@somewhere.travel',
            'address@somewhere.ventures',
            'address@somewhere.viajes',
            'address@somewhere.voyage',
            'address@somewhere.한국',
            'address@somewhere.公益',
            'address@somewhere.қаз',
            'address@somewhere.онлайн',
            'address@somewhere.сайт',
            'address@somewhere.срб',
            'address@somewhere.中国',
            'address@somewhere.中國',
            'address@somewhere.भारत',
            'address@somewhere.укр',
            'address@somewhere.香港',
            'address@somewhere.台湾',
            'address@somewhere.台灣',
            'address@somewhere.мон',
            'address@somewhere.рф',
            'address@somewhere.游戏',
            'address@somewhere.新加坡',
            'address@somewhere.政务',
        ];
        return array_combine(
            $emails,
            array_map(
                static fn(string $email) => [$email],
                $emails
            )
        );
    }

    public function provideInvalidEmails(): array
    {
        $emails = [
            '',
            'address..@email.com',
            '@email.somewhere.com',
            '@email.somewhere.comcom',
            '_address@email.com',
            '.address@email.com',
            'other:adr@email.somewhere.com',
            'other,adr@email.somewhere.com',
        ];
        return array_combine(
            $emails,
            array_map(
                static fn(string $email) => [$email],
                $emails
            )
        );
    }

    public function provideValidEmails(): array
    {
        $emails = [
            'address@email.com',
            'address@email.com',
            'address@email.com',
            'address@some-email.com',
            'address@email.com',
            'other.address@email.somewhere.com',
            'address@email.com',
            'address@i.com',
        ];
        return array_combine(
            $emails,
            array_map(
                static fn(string $email) => [$email],
                $emails
            )
        );
    }

    /**
     * not very useful, because list of domains increasing
     * https://en.wikipedia.org/wiki/List_of_Internet_top-level_domains#A
     * @dataProvider provideEmailsWithGenericDomain
     */
    public function testEmailsWithGenericDomain(string $email): void
    {
        $this->tester->assertTrue(EmailValidator::isEmail($email));
    }

    /**
     * @dataProvider provideInvalidEmails
     */
    public function testInvalidEmails(string $email): void
    {
        $this->tester->assertFalse(EmailValidator::isEmail($email));
    }

    public function testParseValidEmails(): void
    {
        $this->tester->assertEquals(
            ['address@email.com', 'address2@email.com'],
            EmailValidator::parseValidEmails("address@email.com, \n invalid address@email.com, address2@email.com")
        );
    }

    /**
     * @dataProvider provideValidEmails
     */
    public function testValidEmails(string $email): void
    {
        $this->tester->assertTrue(EmailValidator::isEmail($email));
    }
}
