<?php

declare(strict_types=1);

namespace Tests\Unit;

use Codeception\Test\Unit;
use DateTime;
use DateTimeImmutable;
use DateTimeZone;
use Tests\Support\UnitTester;
use ZdenekGebauer\Utils\Vars\DateUtils;

class DateUtilsTest extends Unit
{
    protected UnitTester $tester;

    public function testCreateUtcNow(): void
    {
        $now = DateUtils::createUtcNow();
        $this->tester->assertEquals(date('Y-m-d H:i:'), $now->format('Y-m-d H:i:'));
        $this->tester->assertEquals(new DateTimeZone('UTC'), $now->getTimezone());
    }

    public function testCreateUtcNowImmutable(): void
    {
        $now = DateUtils::createUtcNowImmutable();
        $this->tester->assertEquals(date('Y-m-d H:i:'), $now->format('Y-m-d H:i:'));
        $this->tester->assertEquals(new DateTimeZone('UTC'), $now->getTimezone());
    }

    public function testDateImmutable(): void
    {
        $now = new DateTime('now', new DateTimeZone('Europe/Prague'));

        $immutable = DateUtils::getDateImmutable($now);
        $this->tester->assertInstanceOf(DateTimeImmutable::class, $immutable);

        $now = new DateTimeImmutable('now', new DateTimeZone('Europe/Prague'));
        $immutable = DateUtils::getDateImmutable($now);
        $this->tester->assertInstanceOf(DateTimeImmutable::class, $immutable);
        $this->tester->assertNotSame($immutable, $now);

        $this->tester->assertNull(DateUtils::getDateImmutable(null));
    }

    public function testDateMutable(): void
    {
        $now = new DateTimeImmutable('now', new DateTimeZone('Europe/Prague'));
        $mutable = DateUtils::getDateMutable($now);
        $this->tester->assertInstanceOf(DateTime::class, $mutable);

        $now = new DateTime('now', new DateTimeZone('Europe/Prague'));
        $mutable = DateUtils::getDateMutable($now);
        $this->tester->assertInstanceOf(DateTime::class, $mutable);
        $this->tester->assertNotSame($mutable, $now);

        $this->tester->assertNull(DateUtils::getDateMutable(null));
    }

    public function testFormat(): void
    {
        $this->tester->assertEquals('', DateUtils::format(null, 'Y-m-d'));
        $datetime = new DateTime('2019-12-31T10:20:30', new DateTimeZone('UTC'));
        $this->tester->assertEquals('2019-12-31T10:20:30+00:00', DateUtils::format($datetime, 'c'));
        $this->tester->assertEquals(
            '8.1.2019 10:20',
            DateUtils::format(new DateTime('2019-01-08T10:20:30'), 'j.n.Y H:i')
        );
    }

    public function testToDateTime(): void
    {
        $this->tester->assertNull(DateUtils::toDateTime(' '));
        $this->tester->assertEquals(
            new DateTime('2019-10-12 10:20:30'),
            DateUtils::toDateTime(' 12. 10. 2019 10:20:30')
        );

        $this->tester->assertNull(DateUtils::toDateTime('invalid'));
        $this->tester->assertNull(DateUtils::toDateTime('0'));
    }

    public function testWithTimeZoneUtc(): void
    {
        $this->tester->assertNull(DateUtils::withTimezoneUtc(null));

        $this->tester->assertEquals(
            new DateTime('2023-10-20 08:20:30', new DateTimeZone('UTC')),
            DateUtils::withTimezoneUtc(new DateTime('2023-10-20 10:20:30', new DateTimeZone('Europe/Prague')))
        );
    }
}
