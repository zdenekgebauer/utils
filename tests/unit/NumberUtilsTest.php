<?php

declare(strict_types=1);

namespace Tests\Unit;

use Codeception\Test\Unit;
use Tests\Support\UnitTester;
use ZdenekGebauer\Utils\Vars\NumberUtils;

class NumberUtilsTest extends Unit
{
    protected UnitTester $tester;

    public function testBytesFromSuffix(): void
    {
        $this->tester->assertEquals(1024, NumberUtils::bytesFromSuffix('1K'));
        $this->tester->assertEquals(3072, NumberUtils::bytesFromSuffix('3K'));
        $this->tester->assertEquals(1_048_576, NumberUtils::bytesFromSuffix('1M'));
        $this->tester->assertEquals(1_073_741_824, NumberUtils::bytesFromSuffix('1G'));
        $this->tester->assertEquals(10, NumberUtils::bytesFromSuffix('10'));
        $this->tester->assertEquals(0, NumberUtils::bytesFromSuffix(''));
    }

    public function testFilterIntPositive(): void
    {
        $this->tester->assertEquals([1, 2], NumberUtils::filterIntPositive([-1, 0, 1, 2]));
        $this->tester->assertEquals([], NumberUtils::filterIntPositive([-1, 0]));
    }

    public function testFloatval(): void
    {
        $this->tester->assertEquals(0, NumberUtils::floatval(''));
        $this->tester->assertEquals(123.45, NumberUtils::floatval('1 2 3,4 5'));
        $this->tester->assertEquals(-123.45, NumberUtils::floatval('- 1 2 3,4 5'));
    }

    public function testFloatvalPositive(): void
    {
        $this->tester->assertEquals(0, NumberUtils::floatvalPositive(''));
        $this->tester->assertEquals(0, NumberUtils::floatvalPositive(0));
        $this->tester->assertEquals(123.45, NumberUtils::floatvalPositive('1 2 3,4 5'));
        $this->tester->assertEquals(0, NumberUtils::floatvalPositive('-1'));
        $this->tester->assertEquals(0, NumberUtils::floatvalPositive('- 1 2 3,4 5'));
    }

    public function testFormatIso(): void
    {
        $this->tester->assertEquals(123, NumberUtils::formatIso(123, 0));
        $this->tester->assertEquals(123.00, NumberUtils::formatIso(123, 2));
        $this->tester->assertEquals('1&#8239;234.560', NumberUtils::formatIso('1234,56', 3));
        $this->tester->assertEquals('1 234.560', NumberUtils::formatIso('1234,56', 3, ' '));
    }

    public function testHumanFileSize(): void
    {
        $this->tester->assertEquals('512.00 B', NumberUtils::humanFilesize(512));
        $this->tester->assertEquals('1.00 KiB', NumberUtils::humanFilesize(1024));
        $this->tester->assertEquals('1.00 MiB', NumberUtils::humanFilesize(1_048_576));
        $this->tester->assertEquals('1.00 GiB', NumberUtils::humanFilesize(1_073_741_824));

        $this->tester->assertEquals('500.00 B', NumberUtils::humanFilesize(500, true));
        $this->tester->assertEquals('1.00 kB', NumberUtils::humanFilesize(1000, true));
        $this->tester->assertEquals('1.00 MB', NumberUtils::humanFilesize(1_000_000, true));
        $this->tester->assertEquals('1.00 GB', NumberUtils::humanFilesize(1_000_000_000, true));

        $this->tester->assertEquals('12,345678 MB', NumberUtils::humanFilesize(12_345_678, true, 6, ','));
        // 2147483647 PHP_INT_MAX in 32bit systems
        $this->tester->assertEquals('2,147484 GB', NumberUtils::humanFilesize(2_147_483_647, true, 6, ','));
        $this->tester->assertEquals('2.000000 GiB', NumberUtils::humanFilesize(2_147_483_647, false, 6));

        $this->tester->assertEquals('0.0 B', NumberUtils::humanFilesize(-1, false, 1));
    }

    public function testIntval(): void
    {
        $this->tester->assertEquals(0, NumberUtils::intval(''));
        $this->tester->assertEquals(0, NumberUtils::intval(0));
        $this->tester->assertEquals(1, NumberUtils::intval('1,9'));
        $this->tester->assertEquals(-1, NumberUtils::intval('-1.1'));
        $this->tester->assertEquals(123456, NumberUtils::intval('123 456,7'));
        $this->tester->assertEquals(-123456, NumberUtils::intval('- 123 456,7'));
        $this->tester->assertEquals(PHP_INT_MAX, NumberUtils::intval(PHP_INT_MAX));
        $this->tester->assertEquals(PHP_INT_MAX, NumberUtils::intval(PHP_INT_MAX + 1));
        $this->tester->assertEquals(PHP_INT_MIN, NumberUtils::intval(PHP_INT_MIN));
        $this->tester->assertEquals(PHP_INT_MIN, NumberUtils::intval(PHP_INT_MIN - 1));
    }

    public function testIntvalPositive(): void
    {
        $this->tester->assertEquals(0, NumberUtils::intvalPositive(''));
        $this->tester->assertEquals(0, NumberUtils::intvalPositive(0));
        $this->tester->assertEquals(1, NumberUtils::intvalPositive('1,9'));
        $this->tester->assertEquals(0, NumberUtils::intvalPositive('-1.1'));
        $this->tester->assertEquals(PHP_INT_MAX, NumberUtils::intvalPositive(PHP_INT_MAX));
        $this->tester->assertEquals(PHP_INT_MAX, NumberUtils::intvalPositive(PHP_INT_MAX + 1));
    }

    public function testIntvalPositiveArray(): void
    {
        $this->tester->assertEquals([], NumberUtils::intvalPositiveArray([]));
        $this->tester->assertEquals([2, 1], NumberUtils::intvalPositiveArray([2, 0, -1, null, 1]));
    }

}
