<?php

declare(strict_types=1);

namespace Unit;

use Codeception\Test\Unit;
use Tests\Support\UnitTester;
use ZdenekGebauer\Utils\Vars\DateTimeImmutableUtc;
use ZdenekGebauer\Utils\Vars\DateTimeUtc;

class DateTimeUtcTest extends Unit
{
    protected UnitTester $tester;

    public function testDateTimeImmutableUtc(): void
    {
        $now = new DateTimeImmutableUtc();
        $this->tester->assertEquals('UTC', $now->getTimezone()->getName());
    }

    public function testDateTimeUtc(): void
    {
        $now = new DateTimeUtc();
        $this->tester->assertEquals('UTC', $now->getTimezone()->getName());
    }
}
