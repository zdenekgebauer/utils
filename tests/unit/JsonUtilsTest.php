<?php

declare(strict_types=1);

namespace Unit;

use Codeception\Test\Unit;
use JsonException;
use stdClass;
use Tests\Support\UnitTester;
use ZdenekGebauer\Utils\Vars\JsonUtils;

class JsonUtilsTest extends Unit
{
    protected UnitTester $tester;

    public function testDecodeObject(): void
    {
        $obj = new stdClass();
        $obj->key = 'value';

        $this->tester->assertEquals($obj, JsonUtils::decodeObject('{"key":"value"}'));
        $this->tester->assertEquals(new stdClass(), JsonUtils::decodeObject(''));
        $this->tester->assertEquals(new stdClass(), JsonUtils::decodeObject(null));
    }

    public function testDecodeObjectInvalid(): void
    {
        $this->tester->assertEquals(new stdClass(), JsonUtils::decodeObject('{invalid'));

        $this->tester->expectThrowable(JsonException::class, static function () {
            JsonUtils::decodeObject('{invalid', true);
        });
    }

    public function testEncodeObject(): void
    {
        $obj = new stdClass();
        $obj->key = 'value';

        $this->tester->assertEquals('{"key":"value"}', JsonUtils::encodeObject($obj));
    }

    public function testEncodeObjectInvalid(): void
    {
        $obj = new stdClass();
        $obj->key = INF;
        $this->tester->assertEquals('', JsonUtils::encodeObject($obj));

        $this->tester->expectThrowable(JsonException::class, static function () use ($obj) {
            JsonUtils::encodeObject($obj, true);
        });
    }

}
