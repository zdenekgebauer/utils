<?php

declare(strict_types=1);

namespace Tests\Unit;

use Codeception\Test\Unit;
use Tests\Support\UnitTester;
use ZdenekGebauer\Utils\Tree\Node;
use ZdenekGebauer\Utils\Tree\Tree;

class TreeTest extends Unit
{
    protected UnitTester $tester;

    public function testChildren(): void
    {
        $tree = new Tree($this->getTestNodes());
        $this->tester->assertEquals([new Node(1, 0)], $tree->children(0));
        $this->tester->assertEquals([new Node(4, 2), new Node(5, 2)], $tree->children(2));
    }

    private function getTestNodes(): array
    {
        return [
            new Node(1, 0),
            new Node(2, 1),
            new Node(3, 1),
            new Node(4, 2),
            new Node(5, 2),
        ];
    }

    public function testGetAllChildren(): void
    {
        $nodes = [
            new Node(1, 0),
            new Node(2, 1),
            new Node(3, 1),
            new Node(4, 2),
            new Node(5, 2),
        ];
        $tree = new Tree($nodes);
        foreach ($nodes as $node) {
            $this->tester->assertContains($node, $tree->getAllChildren(0));
        }
        $this->tester->assertNotContains($nodes[0], $tree->getAllChildren(2));
        $this->tester->assertNotContains($nodes[1], $tree->getAllChildren(2));
        $this->tester->assertNotContains($nodes[2], $tree->getAllChildren(2));
    }

    public function testGetNodes(): void
    {
        $tree = new Tree($this->getTestNodes());
        $this->tester->assertEquals($this->getTestNodes(), $tree->getNodes());
        $this->tester->assertEquals(new Node(2, 1), $tree->getNodeById(2));
        $this->tester->assertNull($tree->getNodeById(0));
    }

    public function testGetParents(): void
    {
        $tree = new Tree($this->getTestNodes());
        $expect = [
            new Node(2, 1),
            new Node(1, 0),
        ];
        $this->tester->assertEquals($expect, $tree->getParents(4));

        $this->tester->assertEquals([new Node(1, 0)], $tree->getParents(2));
        $this->tester->assertEquals([], $tree->getParents(1));
    }
}
