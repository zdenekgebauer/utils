<?php

declare(strict_types=1);

namespace Tests\Unit;

use Codeception\Test\Unit;
use Tests\Support\UnitTester;
use ZdenekGebauer\Utils\Html\Html;

class HtmlTest extends Unit
{
    protected UnitTester $tester;

    public function testChecked(): void
    {
        $this->tester->assertEquals(' checked', Html::checked(true));
        $this->tester->assertEquals(' checked="checked"', Html::checked(true, true));
        $this->tester->assertEquals('', Html::checked(false));
    }

    public function testDisabled(): void
    {
        $this->tester->assertEquals(' disabled', Html::disabled(true));
        $this->tester->assertEquals(' disabled="disabled"', Html::disabled(true, true));
        $this->tester->assertEquals('', Html::disabled(false));
    }

    public function testEscape(): void
    {
        $this->tester->assertEquals('1&amp;2&quot;3&#039;4&lt;5&gt;6', Html::escape("1&2\"3'4<5>6"));
    }

    public function testHidden(): void
    {
        $this->tester->assertEquals(' hidden', Html::hidden(true));
        $this->tester->assertEquals(' hidden="hidden"', Html::hidden(true, true));
        $this->tester->assertEquals('', Html::hidden(false));
    }

    public function testHtml2Text(): void
    {
        $html = "<html>
<body>
   <h1>Header</h1>
        <p>paragraph</p>
  <table>
        <thead>
 <tr>
 <th>Column A</th>
 <th>Column B</th>
 </tr>
 </thead>
 <tbody>
 <tr>
 <td>value 1</td>
 <td>value 2</td>
</tr>
 <tr>
 <td>value 3</td>
 <td>value 4</td>
</tr>
 </tbody>
 </table>
 </body>
 </html>";
        $expect = 'Header
paragraph
================================================================================
'
            . "Column A | Column B\n"
            . "--------------------------------------------------------------------------------\n"
            . "value 1 | value 2\n"
            . "value 3 | value 4\n"
            . "================================================================================";
        $this->tester->assertEquals($expect, Html::html2Text($html));
    }

    public function testOptions(): void
    {
        $values = ['a' => 'A', 'b' => 'B'];
        $expect = '<option value="a">A</option><option value="b">B</option>';
        $this->tester->assertEquals($expect, Html::options($values));
        $expect = '<option value="a">A</option><option value="b" selected>B</option>';
        $this->tester->assertEquals($expect, Html::options($values, 'b'));
        $expect = '<option value="a" selected>A</option><option value="b" selected>B</option>';
        $this->tester->assertEquals($expect, Html::options($values, ['b', 'a']));
    }

    public function testSelected(): void
    {
        $this->tester->assertEquals(' selected', Html::selected(true));
        $this->tester->assertEquals(' selected="selected"', Html::selected(true, true));
        $this->tester->assertEquals('', Html::selected(false));
    }

    public function testShrinkHtml(): void
    {
        $html = " <h1 class=\"class\" style=\"color:green;\">text</h1> \n    <p style=\"color:green\">text</p> \n \n\n  <p>text</p><p>text</p> / <p>text2</p> ";
        $expect = '<h1 class="class" style="color:green;">text</h1> <p style="color:green">text</p> <p>text</p><p>text</p> / <p>text2</p>';
        $this->tester->assertEquals($expect, Html::shrinkHtml($html));
    }

    public function testStripHtml(): void
    {
        $this->tester->assertEquals('aaa ss', Html::stripHtml('<HTML>aaa<script src=""> ss'));
        $this->tester->assertEquals('aaa ss', Html::stripHtml('<HTML>aaa  ss', true));
    }
}
