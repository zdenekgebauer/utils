<?php

declare(strict_types=1);

Codeception\Util\Autoload::addNamespace('', 'src/Email');
Codeception\Util\Autoload::addNamespace('', 'src/Filesystem');
Codeception\Util\Autoload::addNamespace('', 'src/Html');
Codeception\Util\Autoload::addNamespace('', 'src/Tree');
Codeception\Util\Autoload::addNamespace('', 'src/Vars');
