<?php

declare(strict_types=1);

namespace Tests\Unit;

use Codeception\Test\Unit;
use FilesystemIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Tests\Support\UnitTester;
use ZdenekGebauer\Utils\Filesystem\Log;

class LogTest extends Unit
{
    protected UnitTester $tester;

    private ?string $dir = null;

    public function testLogrotate(): void
    {
        $src = $this->dir . '/file.log';
        file_put_contents($src, '   ');

        Log::logrotate($src, 3);
        clearstatcache();
        $this->tester->assertEquals(0, filesize($src));
        $this->tester->assertFileExists($src . '.' . date('Y-m-d-H-i-s'));
        $this->tester->assertEquals(3, filesize($src . '.' . date('Y-m-d-H-i-s')));
        sleep(1);
        Log::logrotate($src, 3);
        sleep(1);
        Log::logrotate($src, 3);
        sleep(1);
        Log::logrotate($src, 3);
        $files = glob($this->dir . '/file.log.*');
        $this->tester->assertEquals(3, is_countable($files) ? count($files) : 0);

        Log::logrotate($src = $this->dir . '/not-exists.log', 3);
    }

    protected function _after(): void
    {
        $this->deleteTestDir();
    }

    private function deleteTestDir(): void
    {
        clearstatcache();
        if (is_dir($this->dir)) {
            $iterator = new RecursiveDirectoryIterator(
                $this->dir,
                RecursiveDirectoryIterator::SKIP_DOTS | FilesystemIterator::SKIP_DOTS
            );
            foreach (new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::CHILD_FIRST) as $file) {
                if ($file->isDir()) {
                    rmdir($file->getPathname());
                }
                if ($file->isFile()) {
                    unlink($file->getPathname());
                }
            }
            rmdir($this->dir);
        }
    }

    protected function _before(): void
    {
        $this->dir = sys_get_temp_dir() . '/test_filesystem';
        $this->deleteTestDir();
        mkdir($this->dir);
    }

}
