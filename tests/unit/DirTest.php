<?php

declare(strict_types=1);

namespace Unit;

use Codeception\Test\Unit;
use FilesystemIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Tests\Support\UnitTester;
use ZdenekGebauer\Utils\Filesystem\Dir;

class DirTest extends Unit
{
    protected UnitTester $tester;

    private string $dir = '';

    public function testEmptyRecursive(): void
    {
        mkdir($this->dir . '/sub1/sub2', 0777, true);
        $file1 = $this->dir . '/sub1/file1.txt';
        file_put_contents($file1, 'test');

        Dir::emptyRecursive($this->dir, true);

        clearstatcache();
        $this->tester->assertFalse(is_dir($this->dir));
    }

    public function testEmptyRecursiveContentOnly(): void
    {
        mkdir($this->dir . '/sub1/sub2', 0777, true);
        $file1 = $this->dir . '/file1.txt';
        file_put_contents($file1, 'test');
        $file2 = $this->dir . '/sub1/file2.txt';
        file_put_contents($file2, 'test');

        Dir::emptyRecursive($this->dir);

        clearstatcache();
        $this->tester->assertTrue(is_dir($this->dir));
        $this->tester->assertFalse(is_dir($this->dir . '/sub1'));
        $this->tester->assertFalse(is_file($file1));
    }

    public function testMkDir(): void
    {
        $newDir = $this->dir . '/sub1/sub2';
        $this->tester->assertFalse(is_dir($newDir));

        clearstatcache();
        Dir::mkdir($newDir);

        clearstatcache();
        $this->tester->assertTrue(is_dir($newDir));
    }

    protected function _after(): void
    {
        $this->deleteTestDir();
    }

    private function deleteTestDir(): void
    {
        clearstatcache();

        if (is_dir($this->dir)) {
            $iterator = new RecursiveDirectoryIterator(
                $this->dir,
                FilesystemIterator::SKIP_DOTS | FilesystemIterator::SKIP_DOTS
            );
            foreach (new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::CHILD_FIRST) as $file) {
                if ($file->isDir()) {
                    rmdir($file->getPathname());
                }
                if ($file->isFile()) {
                    unlink($file->getPathname());
                }
            }
            rmdir($this->dir);
        }
        clearstatcache();
    }

    protected function _before(): void
    {
        $this->dir = sys_get_temp_dir() . '/test_filesystem';
        $this->deleteTestDir();
        mkdir($this->dir);
    }

}
