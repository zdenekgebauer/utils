<?php

declare(strict_types=1);

namespace ZdenekGebauer\Utils\Html;

class Url
{
    /**
     * build query string from given arrays
     *
     * Returns $baseUrl with query string. Query string  will contain all
     * parameters from arrays given as arguments after $baseUrl.
     * Parameters with value NULL will be ignored.
     *
     * @param array<string, array<int|string>|int|string|true> $parameters assoc arrays with parameter for url
     */
    public static function queryUrl(string $baseUrl, array $parameters, string $argSeparator = '&'): string
    {
        $query = http_build_query($parameters, '', $argSeparator);
        if ($query !== '') {
            $baseUrl .= (str_contains($baseUrl, '?') ? $argSeparator : '?');
        }
        return $baseUrl . $query;
    }

    /**
     * In given html remove absolute url in src and href attributes
     */
    public static function removeAbsUrl(string $text, string $baseUrl): string
    {
        return str_replace([' src="' . $baseUrl, ' href="' . $baseUrl], [' src="', ' href="'], $text);
    }

    public static function toDataUri(string $file): string
    {
        if ($file === '') {
            return '';
        }
        $data = file_get_contents($file);
        return $data === false ? '' : 'data:' . mime_content_type($file) . ';base64,' . base64_encode($data);
    }

    /**
     * In given html replace relative url in src and href attributes with absolute urls starting with $baseUrl
     */
    public static function urlToAbs(string $text, string $baseUrl): string
    {
        return (string)preg_replace(
            '#(href|src)="([^:"]*)("|(?:(?:%20|\s|\+)[^"]*"))#',
            '$1="' . $baseUrl . '$2$3',
            $text
        );
    }
}
