<?php

declare(strict_types=1);

namespace ZdenekGebauer\Utils\Html;

use function in_array;
use function is_array;

class Html
{
    /**
     * returns html attribute ' checked' or empty string
     *
     * @param bool $xhtml return attribute in XHTML format
     */
    public static function checked(bool $condition, bool $xhtml = false): string
    {
        $attr = $xhtml ? ' checked="checked"' : ' checked';
        return $condition ? $attr : '';
    }

    /**
     * returns html attribute 'disabled' or empty string
     * @param bool $xhtml return attribute in XHTML format
     */
    public static function disabled(bool $condition, bool $xhtml = false): string
    {
        $attr = $xhtml ? ' disabled="disabled"' : ' disabled';
        return $condition ? $attr : '';
    }

    /**
     * returns html attribute 'hidden' or empty string
     */
    public static function hidden(bool $condition, bool $xhtml = false): string
    {
        $attr = $xhtml ? ' hidden="hidden"' : ' hidden';
        return $condition ? $attr : '';
    }

    public static function html2Text(string $html): string
    {
        $text = $html;
        if (preg_match("/<body[^>]*>(.*?)<\/body>/is", $html, $matches) > 0) {
            $text = $matches[1];
        }

        $text = self::pregReplace('/<table(.*)>\R/m', str_repeat('=', 80) . "\n", $text);
        $text = str_replace('</table>', "\n" . str_repeat('=', 80), $text);
        $text = self::pregReplace('/<\/td>\s*?<td/m', '</td> | <td', $text);
        $text = self::pregReplace('/<\/th>\s*?<th/m', '</th> | <th', $text);
        $text = self::pregReplace('/<\/thead>\R/m', str_repeat('-', 80) . "\n", $text);
        $text = self::pregReplace('/<tfoot>\R/m', str_repeat('-', 80) . "\n", $text);
        $text = self::pregReplace('/<thead>\R|<tbody>\R|<\/tbody>\R|<\/tfoot>\R|<tr[^\>]*>\R|<\/tr>\R/m', '', $text);
        $text = trim(html_entity_decode(strip_tags($text), ENT_QUOTES | ENT_HTML5, 'UTF-8'));
        // remove leading whitespaces
        $text = self::pregReplace('/^[ \t]+/m', '', $text);

        while (str_contains($text, "\n\n")) {
            $text = str_replace("\n\n", "\n", $text);
        }
        return $text;
    }

    private static function pregReplace(string $pattern, string $replacement, string $subject): string
    {
        return (string)preg_replace($pattern, $replacement, $subject);
    }

    /**
     * returns html <option> tags created from given array. Expects array in format array[$key] = $value
     *
     * @param array<int|string> $options
     * @param array<int|string>|int|string|null $selectedKey selected key(s)
     */
    public static function options(array $options, array|int|string|null $selectedKey = null): string
    {
        $ret = '';
        if (!is_array($selectedKey)) {
            $selectedKey = [$selectedKey];
        }
        foreach ($options as $key => $value) {
            $ret .= '<option value="' . self::escape((string)$key) . '"' .
                self::selected(in_array($key, $selectedKey, false)) . '>' . self::escape((string)$value) . '</option>';
        }
        return $ret;
    }

    /**
     * returns escaped string for html
     */
    public static function escape(string $text): string
    {
        return htmlspecialchars($text, ENT_QUOTES);
    }

    /**
     * returns html attribute ' selected' or empty string
     *
     * @param bool $xhtml return attribute in XHTML format
     */
    public static function selected(bool $condition, bool $xhtml = false): string
    {
        $attr = $xhtml ? ' selected="selected"' : ' selected';
        return $condition ? $attr : '';
    }

    public static function shrinkHtml(string $html): string
    {
        $search = [
            '/\>[^\S ]+/', // strip whitespaces after tags, except space
            '/[^\S ]+\</', // strip whitespaces before tags, except space
            '/(\s)+/',  // shorten multiple whitespace sequences
        ];
        $replace = ['>', '<', '\\1'];
        return trim((string)preg_replace($search, $replace, $html));
    }

    /**
     * remove problematic html tags and invisible content
     */
    public static function stripHtml(string $text, bool $removeWhitespaces = false): string
    {
        $text = self::addLineBreaks(self::stripInvisibleTags($text));

        $text = html_entity_decode(strip_tags($text), ENT_QUOTES);
        if ($removeWhitespaces) {
            $text = (string)preg_replace(['/(\s)+/u', '/(\s)+/'], [' ', '\\1'], $text);
        }
        return trim($text);
    }

    /**
     * add line breaks before and after block tags
     */
    public static function addLineBreaks(string $text): string
    {
        $blockTags = [
            '~<((br)|(hr))~iu',
            '~</?((address)|(blockquote)|(center)|(del))~iu',
            '~</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))~iu',
            '~</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))~iu',
            '~</?((table)|(th)|(td)|(caption))~iu',
            '~</?((form)|(button)|(fieldset)|(legend)|(input))~iu',
            '~</?((label)|(select)|(optgroup)|(option)|(textarea))~iu',
            '~</?((frameset)|(frame)|(iframe))~iu',
        ];
        return (string)preg_replace($blockTags, "\n\$0", $text);
    }

    public static function stripInvisibleTags(string $text): string
    {
        $removeTags = [
            '~<head[^>]*?>.*?</head>~siu',
            '~<style[^>]*?>.*?</style>~siu',
            '~<script[^>]*?.*?</script>~siu',
            '~<object[^>]*?.*?</object>~siu',
            '~<embed[^>]*?.*?</embed>~siu',
            '~<applet[^>]*?.*?</applet>~siu',
            '~<noframes[^>]*?.*?</noframes>~siu',
            '~<noscript[^>]*?.*?</noscript>~siu',
            '~<noembed[^>]*?.*?</noembed>~siu',
        ];
        return (string)preg_replace($removeTags, ' ', $text);
    }
}
