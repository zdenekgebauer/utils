<?php

declare(strict_types=1);

namespace ZdenekGebauer\Utils\Email;

class EmailValidator
{
    public static function isEmail(?string $email): bool
    {
        // period and underscore may not be used to start or end the
        // local part, nor may two or more consecutive periods appear
        if ($email === '' || $email === null || str_starts_with($email, '.') || str_starts_with($email, '_')) {
            return false;
        }

        foreach (['.@', '..', ':', ','] as $value) {
            if (str_contains($email, $value)) {
                return false;
            }
        }

        // filter_var do not validate non-latin chars
        if (iconv('UTF-8', 'ISO-8859-1//IGNORE', $email) === $email) {
            return (filter_var($email, FILTER_VALIDATE_EMAIL) !== false);
        }

        // phpcs:ignore
        $regexp = '/^[a-z0-9!#$%&*+-=?^_`{|}~]+(\.[a-z0-9!#$%&*+-=?^_`{|}~]+)*@([-a-z0-9]+\.)+(한국|公益|қаз|онлайн|сайт|срб|中国|中國|भारत|укр|香港|台湾|台灣|мон|рф|游戏|新加坡|政务|)$/ixu';
        return (bool)preg_match($regexp, $email);
    }

    /**
     * @param array<string> $delimiters
     * @return array<string>
     */
    public static function parseValidEmails(string $text, array $delimiters = [' ', ',', "\n"]): array
    {
        $defaultDelimiter = ' ';
        $text = str_replace($delimiters, $defaultDelimiter, $text);
        $emails = explode($defaultDelimiter, $text);
        $emails = array_map('trim', $emails);
        return array_values(array_unique(array_filter($emails, self::isEmail(...))));
    }
}
