<?php

declare(strict_types=1);

namespace ZdenekGebauer\Utils\Tree;

class Node
{
    public function __construct(public int $id = 0, public int $parentId = 0)
    {
    }
}
