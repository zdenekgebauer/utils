<?php

declare(strict_types=1);

namespace ZdenekGebauer\Utils\Tree;

/**
 * common tree structure
 */
class Tree
{
    /**
     * @var array<Node>
     */
    protected array $nodes;

    /**
     * @param array<Node> $items
     */
    public function __construct(array $items)
    {
        array_walk(
            $items,
            static function (Node $item) {
            }
        );
        $this->nodes = $items;
    }

    /**
     * returns children of given node
     *
     * @return array<Node>
     */
    public function children(int $nodeId): array
    {
        $ret = [];
        foreach ($this->nodes as $item) {
            if ($item->parentId === $nodeId) {
                $ret[] = $item;
            }
        }
        return $ret;
    }

    /**
     * @return array<Node>
     */
    public function getAllChildren(int $nodeId): array
    {
        $ret = [];
        foreach ($this->nodes as $item) {
            if ($item->parentId === $nodeId) {
                $ret[] = $item;
                $ret = array_merge($ret, $this->getAllChildren($item->id));
            }
        }
        return $ret;
    }

    /**
     * @return array<Node>
     */
    public function getNodes(): array
    {
        return $this->nodes;
    }

    /**
     * returns parent items of item with given id
     *
     * @return array<Node>
     */
    public function getParents(int $nodeId): array
    {
        $ret = [];
        $tmp = $this->getNodeById($nodeId);
        while ($tmp) {
            if ($tmp->parentId === 0) {
                break;
            }
            $tmp = $this->getNodeById($tmp->parentId);
            if ($tmp instanceof Node) {
                $ret[] = $tmp;
            }
        }
        return $ret;
    }

    public function getNodeById(int $nodeId): ?Node
    {
        foreach ($this->nodes as $item) {
            if ($item->id === $nodeId) {
                return $item;
            }
        }
        return null;
    }
}
