<?php

declare(strict_types=1);

namespace ZdenekGebauer\Utils\Vars;

use DateTime;
use DateTimeZone;

class DateTimeUtc extends DateTime
{
    public function __construct(string $datetime = 'now')
    {
        parent::__construct($datetime, new DateTimeZone('UTC'));
    }
}
