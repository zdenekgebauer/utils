<?php

declare(strict_types=1);

namespace ZdenekGebauer\Utils\Vars;

use JsonException;
use stdClass;

class JsonUtils
{
    /**
     * @throws JsonException
     */
    public static function decodeObject(?string $json, bool $throwOnError = false): stdClass
    {
        $result = new stdClass();
        if ($json === '' || $json === null) {
            return $result;
        }

        try {
            /** @phpstan-var stdClass $result */
            $result = json_decode($json, false, 512, JSON_THROW_ON_ERROR | JSON_FORCE_OBJECT);
        } catch (JsonException $exception) {
            if ($throwOnError) {
                throw $exception;
            }
        }
        return $result;
    }

    /**
     * @throws JsonException
     */
    public static function encodeObject(stdClass $value, bool $throwOnError = false): string
    {
        try {
            return json_encode($value, JSON_FORCE_OBJECT | JSON_THROW_ON_ERROR);
        } catch (JsonException $exception) {
            if ($throwOnError) {
                throw $exception;
            }
        }
        return '';
    }
}
