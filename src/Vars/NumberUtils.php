<?php

declare(strict_types=1);

namespace ZdenekGebauer\Utils\Vars;

/**
 * functions for convert and format numbers
 */
final class NumberUtils
{
    public static function bytesFromSuffix(string $value): int
    {
        static $map = ['k' => 1024, 'm' => 1_048_576, 'g' => 1_073_741_824];
        $suffix = strtolower(substr($value, -1));
        return (int)$value * ($map[$suffix] ?? 1);
    }

    /**
     * @param array<int> $values
     * @return array<int>
     */
    public static function filterIntPositive(array $values): array
    {
        return array_values(
            array_filter(
                array_map(self::intvalPositive(...), $values),
                static fn(int $value) => $value > 0
            )
        );
    }

    public static function floatvalPositive(float|string $number): float
    {
        return max([0, self::floatval($number)]);
    }

    public static function floatval(float|string $number): float
    {
        return (float)trim(str_replace([' ', ','], ['', '.'], (string)$number));
    }

    /**
     * returns number formatted by ISO 31-0 with dot as decimal point
     *
     * @param int $decimals number of digits after decimal point
     * @param string $thousandsDelimiter default `narrow non-breaking space`
     * @see http://en.wikipedia.org/wiki/ISO_31-0#Numbers
     */
    public static function formatIso(string|float $value, int $decimals, string $thousandsDelimiter = '&#8239;'): string
    {
        return number_format(self::floatval($value), $decimals, '.', $thousandsDelimiter);
    }

    /**
     * returns a human-readable file size with binary or decimal suffix. Doesn't work correctly for file size > 2GiB
     * @param bool $decimalSuffix TRUE|FALSE - return decimal|binary suffix
     * @param int $decimals number of digits after decimal point
     * @param string $decPoint string representing decimal point
     * @see https://en.wikipedia.org/wiki/File_size
     */
    public static function humanFilesize(
        int $size,
        bool $decimalSuffix = false,
        int $decimals = 2,
        string $decPoint = '.'
    ): string {
        $size = (max($size, 0));

        $prefixes = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB'];
        $divider = 1024;
        if ($decimalSuffix) {
            $prefixes = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB'];
            $divider = 1000;
        }

        $iteration = 0;
        while (($size / $divider) >= 1) {
            $size /= $divider;
            $iteration++;
        }
        return number_format($size, $decimals, $decPoint, '') . ' ' . $prefixes[$iteration];
    }

    public static function intvalPositive(bool|float|int|null|string $number): int
    {
        return max([0, self::intval($number)]);
    }

    public static function intval(bool|float|int|null|string $number): int
    {
        return (int)trim(str_replace([' ', ','], ['', '.'], (string)$number));
    }

    /**
     * @param array<bool|int|float|string> $values
     * @return array<int>
     */
    public static function intvalPositiveArray(array $values): array
    {
        return array_values(array_filter(array_map(self::intvalPositive(...), $values)));
    }
}
