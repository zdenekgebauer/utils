<?php

declare(strict_types=1);

namespace ZdenekGebauer\Utils\Vars;

use DateTimeImmutable;
use DateTimeZone;

class DateTimeImmutableUtc extends DateTimeImmutable
{
    public function __construct(string $datetime = 'now')
    {
        parent::__construct($datetime, new DateTimeZone('UTC'));
    }
}
