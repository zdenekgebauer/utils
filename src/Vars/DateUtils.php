<?php

declare(strict_types=1);

namespace ZdenekGebauer\Utils\Vars;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Exception;

class DateUtils
{
    public static function createUtcNow(): DateTime
    {
        return new DateTime('now', new DateTimeZone('UTC'));
    }

    public static function createUtcNowImmutable(): DateTimeImmutable
    {
        return new DateTimeImmutable('now', new DateTimeZone('UTC'));
    }

    public static function format(?DateTimeInterface $dateTime, string $format): string
    {
        return $dateTime instanceof DateTimeInterface ? $dateTime->format($format) : '';
    }

    public static function getDateImmutable(?DateTimeInterface $dateTime): ?DateTimeImmutable
    {
        if ($dateTime instanceof DateTimeInterface) {
            return DateTimeImmutable::createFromInterface($dateTime);
        }
        return null;
    }

    public static function toDateTime(string $value): ?DateTimeImmutable
    {
        $value = trim(str_replace('. ', '.', $value)); // fix "j. n. Y" to "j.n.Y"
        $result = null;
        if ($value !== '') {
            try {
                $result = new DateTimeImmutable($value);
            } catch (Exception) {
                return null;
            }
        }
        return $result;
    }

    public static function withTimezoneUtc(?DateTimeInterface $dateTime): ?DateTime
    {
        return self::withTimezone($dateTime, 'UTC');
    }

    public static function withTimezone(?DateTimeInterface $dateTime, string $timezone): ?DateTime
    {
        if (!$dateTime instanceof DateTimeInterface) {
            return null;
        }
        $result = self::getDateMutable($dateTime);
        $result?->setTimezone(new DateTimeZone($timezone));
        return $result;
    }

    public static function getDateMutable(?DateTimeInterface $dateTime): ?DateTime
    {
        if ($dateTime instanceof DateTime) {
            return clone $dateTime;
        }
        return $dateTime instanceof DateTimeImmutable ? DateTime::createFromImmutable($dateTime) : null;
    }
}
