<?php

declare(strict_types=1);

namespace ZdenekGebauer\Utils\Vars;

use Exception;

use function function_exists;
use function strlen;

class StringUtils
{
    /**
     * from given text parse words with minimal length
     *
     * @return array<string>
     */
    public static function keywords(string $string, int $minLength = 5): array
    {
        $words = [];
        $minLength = ($minLength > 2 && $minLength < 50 ? $minLength : 5);
        $tmp = explode(' ', $string);
        foreach ($tmp as $word) {
            $trans = [',' => '', '"' => ''];
            $word = trim(strtr($word, $trans));
            if (self::strlen($word) >= $minLength) {
                $words[] = $word;
            }
        }
        return $words;
    }

    /**
     * returns length of multibyte string
     */
    public static function strlen(string $string, string $encoding = 'UTF-8'): int
    {
        if (function_exists('mb_strlen')) {
            return mb_strlen($string, $encoding);
        }
        return function_exists('iconv_strlen') ? (int)iconv_strlen($string, $encoding) : strlen($string);
    }

    public static function maxLength(string $string, int $length): string
    {
        return self::substr($string, 0, $length);
    }

    public static function substr(string $string, int $start, ?int $length = null, string $encoding = 'UTF-8'): string
    {
        $length ??= self::strlen($string);
        if (function_exists('mb_substr')) {
            return mb_substr($string, $start, $length, $encoding);
        }
        return function_exists('iconv_substr')
            ? (string)iconv_substr($string, $start, $length, $encoding) : substr($string, $start, $length);
    }

    /**
     * returns first part of string with specified length with wordwrap
     */
    public static function perex(string $string, int $length, ?int $over = null, string $suffix = ''): string
    {
        $string = htmlspecialchars_decode($string, ENT_QUOTES);
        $string = html_entity_decode($string, ENT_QUOTES, 'UTF-8');
        $tmp = wordwrap(trim($string), $length, '[DELIMITER]');
        $tmp = explode('[DELIMITER]', $tmp);
        $ret = $tmp[0];
        if ($over > 0 && (self::strlen($ret) > $length + $over)) {
            $ret = self::substr($ret, 0, $length + $over);
        }
        if (self::strlen($ret) < self::strlen($string)) {
            $ret .= $suffix;
        }
        return $ret;
    }

    /**
     * returns random alphanumeric string (only ascii chars and digits)
     * @param int $length of required string
     * @throws Exception
     */
    public static function randomString(int $length): string
    {
        $chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789abcdefghjklmnpqrstuvwxyz';
        $ret = '';
        for ($counter = 0; $counter < $length; $counter++) {
            $ret .= $chars[random_int(0, strlen($chars) - 1)];
        }
        return $ret;
    }

    /**
     * return string with removed accents and all chars except [a-z0-9_] and additional allowed chars replaced
     * with hyphen
     */
    public static function slugify(string $string): string
    {
        $convert = [
            'á' => 'a',
            'Á' => 'a',
            'ä' => 'a',
            'Ä' => 'a',
            'ą' => 'a',
            'Ą' => 'a',
            'č' => 'c',
            'Č' => 'c',
            'ď' => 'd',
            'Ď' => 'd',
            'é' => 'e',
            'É' => 'e',
            'ě' => 'e',
            'Ě' => 'e',
            'ę' => 'e',
            'Ę' => 'e',
            'ē' => 'e',
            'Ē' => 'e',
            'ë' => 'e',
            'Ë' => 'e',
            'í' => 'i',
            'Í' => 'i',
            'ľ' => 'l',
            'Ľ' => 'l',
            'ł' => 'l',
            'Ł' => 'l',
            'ĺ' => 'l',
            'Ĺ' => 'l',
            'ň' => 'n',
            'Ň' => 'n',
            'ó' => 'o',
            'Ó' => 'o',
            'ö' => 'o',
            'Ö' => 'o',
            'ő' => 'o',
            'Ő' => 'o',
            'ř' => 'r',
            'Ř' => 'r',
            'š' => 's',
            'Š' => 's',
            'ť' => 't',
            'Ť' => 't',
            'ú' => 'u',
            'Ú' => 'u',
            'ů' => 'u',
            'Ů' => 'u',
            'ű' => 'u',
            'Ű' => 'u',
            'ü' => 'u',
            'Ü' => 'u',
            'ý' => 'y',
            'Ý' => 'y',
            'ž' => 'z',
            'Ž' => 'z',
        ];
        $string = strtolower(strtr($string, $convert));
        $string = (string)preg_replace('/[^a-z0-9_]+/', '-', $string);
        return trim($string, '-');
    }

    /**
     * returns string padded and cropped to exact length
     * @param int $length length of returned string
     * @param string $padString padding character
     * @param int $padType one of STR_PAD_* constants
     */
    public static function strPadCut(
        string $string,
        int $length,
        string $padString,
        int $padType = STR_PAD_RIGHT
    ): string {
        return substr(str_pad($string, $length, $padString, $padType), 0, $length);
    }
}
