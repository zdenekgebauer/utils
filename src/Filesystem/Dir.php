<?php

declare(strict_types=1);

namespace ZdenekGebauer\Utils\Filesystem;

use FilesystemIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;

use function assert;

class Dir
{
    public static function emptyRecursive(string $dir, bool $selfDelete = false): void
    {
        if (is_dir($dir)) {
            $iterator = new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS);
            foreach (new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::CHILD_FIRST) as $file) {
                assert($file instanceof SplFileInfo);
                if ($file->isDir()) {
                    rmdir($file->getPathname());
                }
                if ($file->isFile()) {
                    unlink($file->getPathname());
                }
            }
            if ($selfDelete) {
                rmdir($dir);
            }
        }
    }

    public static function mkdir(string $directory, int $permissions = 0777, bool $recursive = true): void
    {
        if (!is_dir($directory)) {
            mkdir($directory, $permissions, $recursive);
        }
        assert(is_dir($directory));
    }
}
