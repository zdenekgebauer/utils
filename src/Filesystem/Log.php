<?php

declare(strict_types=1);

namespace ZdenekGebauer\Utils\Filesystem;

use function count;
use function is_array;
use function is_int;

class Log
{
    /**
     * similar to unix logrotate, copy specified file with timestamp as suffix and delete older logs
     * @param string $fullPath file for rotate
     * @param int $history how many older version to keep
     */
    public static function logrotate(string $fullPath, int $history): void
    {
        if (!is_file($fullPath)) {
            return;
        }

        umask(000);
        copy($fullPath, $fullPath . '.' . date('Y-m-d-H-i-s'));
        file_put_contents($fullPath, '');

        // delete older versions
        $oldLogs = glob($fullPath . '*');
        $oldLogs = (is_array($oldLogs) ? $oldLogs : []);
        $key = array_search($fullPath, $oldLogs, true);
        if (is_int($key)) {
            unset($oldLogs[$key]);
        }
        sort($oldLogs);
        $length = count($oldLogs) - $history;
        for ($counter = 0; $counter < $length; $counter++) {
            unlink($oldLogs[$counter]);
        }
    }
}
